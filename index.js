var seneca = require('seneca')();

seneca.add({role: 'salestax', cmd: 'getById'}, function(msg, respond) {
  var taxes = {
    salestax: 2.46
  };

  respond(null, taxes);
});

seneca.listen();

seneca.ready(function() {
  console.log('salestax-service ready!');
});
